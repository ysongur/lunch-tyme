# LunchTyme

```
/ \   _ __   __ _ _   _| | __ _ _ __     / ___| |   |_ _|
   / △ \ | '_ \ / _` | | | | |/ _` | '__|   | |   | |    | |
  / ___ \| | | | (_| | |_| | | (_| | |      | |___| |___ | |
 /_/   \_\_| |_|\__, |\__,_|_|\__,_|_|       \____|_____|___|
                |___/
    

Angular CLI: 7.3.3
Node: 8.15.0
OS: darwin x64
Angular: 7.2.7
... animations, common, compiler, compiler-cli, core, forms
... language-service, platform-browser, platform-browser-dynamic
... router

Package                           Version
-----------------------------------------------------------
@angular-devkit/architect         0.13.3
@angular-devkit/build-angular     0.13.3
@angular-devkit/build-optimizer   0.13.3
@angular-devkit/build-webpack     0.13.3
@angular-devkit/core              7.3.3
@angular-devkit/schematics        7.3.3
@angular/cdk                      7.3.3
@angular/cli                      7.3.3
@angular/fire                     5.1.1
@angular/flex-layout              7.0.0-beta.23
@ngtools/webpack                  7.3.3
@schematics/angular               7.3.3
@schematics/update                0.13.3
rxjs                              6.3.3
typescript                        3.2.4
webpack                           4.29.0
```

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.3.0.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Build for Deploy
Run `npm run build:prod && firebase deploy`.  Firebase cli tool needed.  

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
