import { Component, EventEmitter, Output, TemplateRef, ViewChild, ViewContainerRef } from '@angular/core'
import {  Overlay, OverlayContainer, OverlayRef } from '@angular/cdk/overlay'
import { TemplatePortal } from '@angular/cdk/portal'
import { first } from 'rxjs/operators'
import { animate, state, style, transition, trigger } from '@angular/animations'

@Component({
  selector: 'lt-drawer',
  templateUrl: './drawer.component.html',
  styleUrls: ['./drawer.component.scss'],
  animations: [
    trigger('openClose', [
      state('open', style({
        right: 0,
      })),
      state('closed', style({
        right: '-100%',
      })),
      transition('open => closed', [
        animate('.35s')
      ]),
      transition('closed => open', [
        animate('.25s')
      ]),
    ])
  ]
})
export class DrawerComponent {
  @ViewChild('drawerContent') public drawerContent: TemplateRef<any>
  @Output() public drawerClosed: EventEmitter<boolean>

  public isOpen: boolean

  private _overlayRef: OverlayRef
  private _drawerPortal: TemplatePortal

  constructor(
    private _overlay: Overlay,
    private _overlayContainer: OverlayContainer,
    private _view: ViewContainerRef
  ) {
    this.drawerClosed = new EventEmitter(false)
  }

  public open() {
    this._overlayRef = this._overlay.create({
      backdropClass: ['cdk-overlay-dark-backdrop', 'drawer-backdrop'],
      panelClass: ['drawer-panel'],
      hasBackdrop: true,
      direction: 'rtl'
    })

    this._drawerPortal = new TemplatePortal(this.drawerContent, this._view)

    this._overlayRef.attach(this._drawerPortal)
    setTimeout(() => this.isOpen = true)
    this._overlayRef.backdropClick()
      .pipe(first())
      .subscribe(() => {
        this.isOpen = false
        setTimeout(() => this.close(), 350)
      })
  }

  public close() {
    if (this.isOpen) {
      this.isOpen = false
      setTimeout(() => this.close(), 350)
    } else if (!!this._overlayRef) {
      this._overlayRef.dispose()
      this.drawerClosed.emit(true)
    }
  }
}
