import {
  Component,
  ElementRef,
  Input,
  OnChanges,
  Renderer2,
  SimpleChanges,
  ViewChild
} from '@angular/core'
import { IRestauraunt } from '../restauraunt.interface'
import {GMapService} from '../gmap.service'
declare let google: any

@Component({
  selector: 'lt-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss'],
  animations: [ ]
})
export class DetailComponent implements OnChanges {
  @Input() public restaurant: IRestauraunt
  @ViewChild('gmap') public gmap: ElementRef
  @ViewChild('container') public container: ElementRef

  constructor(
    private _render: Renderer2,
    private _gmap: GMapService
  ) { }

  public ngOnChanges(changes: SimpleChanges): void {
    if (!!changes.restaurant) {
      this._hideMap()

      if (!changes.restaurant.isFirstChange() && !!changes.restaurant.currentValue) {
        this._handleMapLoad()
      }
    }
  }

  private _handleMapLoad() {
    let checkGoogle
    const maxTries = 20
    let tries = 0

    checkGoogle = setInterval(() => {
      tries++
      if (!!google) {
        const center = {lat: this.restaurant.location.lat, lng: this.restaurant.location.lng}
        const map = this._gmap.makeMap(
          center,
          this.gmap.nativeElement,
          12
        )

        this._gmap.makeMarker(center, map)
        this._showMap()
        clearInterval(checkGoogle)
      } else if (tries >= maxTries) {
        clearInterval(checkGoogle)
      }
    }, 500)
  }

  private _showMap() {
    const container = this.container.nativeElement

    this._render.setStyle(container, 'display', 'block')
    setTimeout( () => {
      this._render.addClass(container, 'show-map')
    })
  }
  
  private _hideMap() {
    const container = this.container.nativeElement

    this._render.setStyle(container, 'display', 'none')
    this._render.removeClass(container, 'show-map')
  }
}
