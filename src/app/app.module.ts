import { BrowserModule } from '@angular/platform-browser'
import { NgModule } from '@angular/core'
import { AngularFireModule } from '@angular/fire'
import { AngularFireDatabaseModule } from '@angular/fire/database'
import { FlexLayoutModule } from '@angular/flex-layout'
import { HttpClientModule } from '@angular/common/http'
import { OverlayModule } from '@angular/cdk/overlay'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'

import { AppComponent } from './app.component'
import { environment } from '../environments/environment'
import { LunchCardComponent } from './lunch-card/lunch-card.component'
import { DetailComponent } from './detail/detail.component'
import { DrawerComponent } from './drawer/drawer.component'

@NgModule({
  declarations: [
    AppComponent,
    LunchCardComponent,
    DetailComponent,
    DrawerComponent,
  ],
  imports: [
    BrowserAnimationsModule,
    BrowserModule,
    FlexLayoutModule,
    HttpClientModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    OverlayModule
  ],
  providers: [ ],
  bootstrap: [ AppComponent ],
  exports: [ BrowserAnimationsModule ]
})
export class AppModule { }
