import { Injectable } from '@angular/core';
import { IRestauraunt } from './restauraunt.interface';
import { BehaviorSubject, Observable } from 'rxjs';

declare let google: any;

export interface IPosition {
  lat: number,
  lng: number
}

export interface IMarkerSelected {
  position: IPosition,
  index: number
}

@Injectable({
  providedIn: 'root'
})
export class GMapService {
  private _markerSelected: BehaviorSubject<IMarkerSelected>
  public readonly markerSelected$: Observable<IMarkerSelected>

  private _markerAttached: BehaviorSubject<any>
  public readonly markerAttached$: Observable<any>

  constructor() {
    this._markerSelected = new BehaviorSubject<IMarkerSelected>(null)
    this.markerSelected$ = this._markerSelected.asObservable()

    this._markerAttached = new BehaviorSubject<any>(null)
    this.markerAttached$ = this._markerAttached.asObservable()
  }

  public makeMap( center: IPosition, mapNativeElement: any, zoom: number): any {
    return new google.maps.Map(mapNativeElement, {center, zoom})
  }


  public makeMarker( position: IPosition, mapRef: any, index?: number): any {
    let label

    if (index) {
      label = {
        text: index.toString(),
        fontSize: '12px',
        color: '#ffffff'
      }
    }

    const marker = new google.maps.Marker({
      position,
      map: mapRef,
      label
    })

    google.maps.event.addListener(marker, 'click', () => {
      this._markerSelected.next({
        position,
        index: Number(index)
      })

      mapRef.setCenter(position)
      if (mapRef.getZoom() < 18) {
        mapRef.setZoom(18)
      }
    })
  }

  public makeMarkers( restaurants: IRestauraunt[], mapRef: any): void {
    restaurants
      .forEach(
        (rest, index) =>
          this._markerAttached.next(
            this.makeMarker({ lat: rest.location.lat, lng: rest.location.lng }, mapRef, ++index)
          )
      )
  }

  public triggerMarker(marker: any): void {
    google.maps.event.trigger(marker, 'click')
  }
}
