import { AfterViewInit, Component, ElementRef, HostListener, Input, OnDestroy, Renderer2, ViewChild } from '@angular/core'
import { MediaObserver } from '@angular/flex-layout'
import { Subscription } from 'rxjs'

@Component({
  selector: 'lt-lunch-card',
  templateUrl: './lunch-card.component.html',
  styleUrls: ['./lunch-card.component.scss']
})
export class LunchCardComponent implements AfterViewInit, OnDestroy {
  @Input() public restaurantImage: string
  @ViewChild('lunchOverlay', {read: ElementRef}) public lunchOverlay: ElementRef
  @ViewChild('lunchImage', {read: ElementRef}) public lunchImage: ElementRef

  private _watchMedia: Subscription

  constructor(
    private _renderer: Renderer2,
    private _media: MediaObserver
  ) { }

  public ngAfterViewInit(): void {
    this._watchMedia = this._media.media$.subscribe( () => this.adjustOverlay())
  }

  public ngOnDestroy(): void {
    this._watchMedia.unsubscribe()
  }

  @HostListener('window:resize', ['$event'])
  public adjustOverlay(): void {
      const img = this.lunchImage.nativeElement
      this._renderer.setStyle(
        this.lunchOverlay.nativeElement,
        'height',
        img.height + 'px',
      )

      this._renderer.setStyle(
        this.lunchOverlay.nativeElement,
        'width',
        img.width + 'px',
      )
  }
}
