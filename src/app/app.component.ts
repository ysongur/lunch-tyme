import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core'
import { AngularFireDatabase } from '@angular/fire/database'
import { Observable, Subscription } from 'rxjs'
import { IRestauraunt } from './restauraunt.interface'
import { DrawerComponent } from './drawer/drawer.component'
import { animate, state, style, transition, trigger } from '@angular/animations'
import { MediaObserver } from '@angular/flex-layout'
import { GMapService, IMarkerSelected } from './gmap.service'
import { first } from 'rxjs/operators'

@Component({
  selector: 'lt-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [
    trigger('openClose', [
      // ...
      state('open', style({
        height: '200px',
        opacity: 1,
        backgroundColor: 'yellow'
      })),
      state('closed', style({
        height: '100px',
        opacity: 0.5,
        backgroundColor: 'green'
      })),
      transition('open => closed', [
        animate('1s')
      ]),
      transition('closed => open', [
        animate('0.5s')
      ]),
    ])
  ]
})
export class AppComponent implements OnInit, OnDestroy {
  public restaurants: Observable<IRestauraunt[]>
  public selectedMarker: Observable<IMarkerSelected>
  public activeRestaurant: IRestauraunt
  public isOpen = false
  public isListView = true

  @ViewChild('drawer') public drawer: DrawerComponent
  @ViewChild('mapView') public mapView: ElementRef

  private _mapViewMarkers: any[ ]
  private _watchMarkers: Subscription

  constructor(
    private _database: AngularFireDatabase,
    public media: MediaObserver,
    public gmap: GMapService
  ) { }

  public ngOnInit(): void {
    this.restaurants = this._database.list('restaurants').valueChanges() as Observable<IRestauraunt[]>

    this.selectedMarker = this.gmap.markerSelected$
    this._mapViewMarkers = [ ]

    // only emits when adding multiple markers for map view
    this._watchMarkers = this.gmap.markerAttached$.subscribe(marker => this._handleNewMultiMarker(marker))
  }

  public ngOnDestroy(): void {
    this._watchMarkers.unsubscribe()
  }

  public handleRestaurantClick(rest: IRestauraunt): void {
    this.drawer.open()
    this.activeRestaurant = rest
    setTimeout( () => this.isOpen = true, 350)
  }

  public handleRestaurantMapViewClick(rest: IRestauraunt, index: number): void {
    this.handleRestaurantClick(rest)
    const marker = this._mapViewMarkers.find( (mapMarker, key) => key === index)
    this.gmap.triggerMarker(marker)
  }

  public showListView(): void {
    this.isListView = true
    this._mapViewMarkers = [ ]
  }

  public showMapView(): void {
    this._mapViewMarkers = [ ]
    this.isListView = false

    this.restaurants
      .pipe(first())
      .subscribe( (restaurants: IRestauraunt[]) => {
        const map = this.gmap.makeMap(
          { lat: restaurants[0].location.lat, lng: restaurants[0].location.lng },
          this.mapView.nativeElement,
          13
        )

        this.gmap.makeMarkers(
          restaurants,
          map
        )
      })
  }

  public handleMapViewClick(): void {
    this.drawer.close()
    setTimeout(() => this.showMapView(), 350)
  }

  public handleListViewClick(): void {
    this.drawer.close()
    setTimeout(() => this.showListView(), 350)
  }

  private _handleNewMultiMarker( marker: any): void {
    if (!!marker) {
      this._mapViewMarkers.push(marker)
    }
  }
}
