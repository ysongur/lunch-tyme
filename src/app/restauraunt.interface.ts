export interface IRestauraunt {
  name: string
  backgroundImageURL: string
  category: string
  contact: {
    phone: string
    formattedPhone: string
    twitter: string
  }
  location: {
    address: string
    crossStreet: string
    lat: number
    lng: number
    postalCode: string
    cc: string
    city: string
    country: string
    formattedAddress: string[],
    state: string
  }
}
