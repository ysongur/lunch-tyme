// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyCVl04r3QhLMmWF9S4gD8Q27gc7wBA1-4A',
    authDomain: 'lunch-tyme-16bbc.firebaseapp.com',
    databaseURL: 'https://lunch-tyme-16bbc.firebaseio.com',
    projectId: 'lunch-tyme-16bbc',
    storageBucket: 'lunch-tyme-16bbc.appspot.com',
    messagingSenderId: '626638380762'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
